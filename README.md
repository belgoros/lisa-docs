# Lisa Project documentation

[MkDocs](https://www.mkdocs.org/) website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## Installation

To be able to run the site locally, see MkDocs [installation](https://www.mkdocs.org/#installation) section for installation details.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
2. Add some new content
3. Generate the website: `mkdocs build` (optional, see more at MkDocs [deploy](https://www.mkdocs.org/#deploying) section).

Read more at MkDocs [documentation](https://www.mkdocs.org/#adding-pages).

## GitLab User or Group Pages

Actually, the generated static web-site is available at https://belgoros.gitlab.io/lisa-docs/.

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `mkdocs.yml`, from `"https://pages.gitlab.io/lisa-docs/"` to
`site_url = "https://namespace.gitlab.io"`.


## Troubleshooting

- [ci](https://about.gitlab.com/gitlab-ci/)
- [mkdocs](http://www.mkdocs.org)
- [install](http://www.mkdocs.org/#installation)
- [documentation](http://www.mkdocs.org)
- [userpages](https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages)
- [project pages](https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages)
